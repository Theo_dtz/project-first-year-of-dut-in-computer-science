package vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;;

/**
 * 
 * Classe qui permet l'affichage de toute l'application
 * et qui contient la fonction main.
 * 
 * @author Theo
 *
 */
public class MainWindow extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Barre de menu de la fen�tre
	 */
	JMenuBar menuBar;
	
	/**
	 *Menu en cascade pour la visualisation 
	 */
	JMenu visualisationMenu;
	
	/**
	 * Menu en cascade pour la saisie
	 */
	JMenu inputMenu;
	
	/*
	 * Item de menu pour quitter l'application
	 */
	JMenu quitItem;
	
	
	
	/**
	 * Constructeur par default de la classe FenetrePrincipale
	 * 
	 * @param parTitre Le titre que l'on veut donner � la fen�tre.
	 */
	public MainWindow (String parTitre){
		
		super(parTitre);
		MainContentPane mainContentPane = new MainContentPane();
		this.setContentPane(mainContentPane);
		setDefaultCloseOperation (EXIT_ON_CLOSE);
		setSize (700,300); setVisible (true); setLocation (700,400);
		
		this.menuBar = new JMenuBar();
		
		this.inputMenu = new JMenu("Nouveau Graphe");
		this.menuBar.add(inputMenu);
		
		this.menuBar.add(new MenuSeparator());
		
		this.visualisationMenu = new JMenu("Choisir un Graphe Existant");
		this.menuBar.add(visualisationMenu);
		
		this.menuBar.add(new MenuSeparator());
		
		this.quitItem = new JMenu("Quitter");
		this.menuBar.add(quitItem);
		
		this.menuBar.add(new MenuSeparator());
		
		this.setJMenuBar(menuBar);	
	}
	
	/**
	 * Fonction main de l'application.
	 * 
	 * @param args Parametres de pr�compialtion.
	 */
	public static void main(String[] args){
		
		MainWindow mainWindow = new MainWindow("Chart Visualisation !");
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	
	
	
	
	

	public JMenuBar getTheMenuBar() { //petit probleme de compatibilit�e avec le get
		return this.menuBar;		  //Nous avons donc renom� la methode avec The pour les diferencier
	}

	public void setMenuBar(JMenuBar menuBar) {
		this.menuBar = menuBar;
	}

	public JMenu getVisualisationMenu() {
		return visualisationMenu;
	}

	public void setVisualisationMenu(JMenu visualisationMenu) {
		this.visualisationMenu = visualisationMenu;
	}

	public JMenu getInputMenu() {
		return inputMenu;
	}

	public void setInputMenu(JMenu inputMenu) {
		this.inputMenu = inputMenu;
	}

	public JMenu getQuitItem() {
		return quitItem;
	}

	public void setQuitItem(JMenu quitItem) {
		this.quitItem = quitItem;
	}
}