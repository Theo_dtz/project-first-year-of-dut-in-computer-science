package vue;

import java.awt.Color;

import javax.swing.JPanel;

/**
 * Classe qui permet de contenir le tableau de donn�es.
 * 
 * @author Theo
 *
 */
public class TablePane extends JPanel {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur par defaut de TablePane
	 */
	public TablePane(){
		
		this.setBackground(Color.GREEN);
	}

}
