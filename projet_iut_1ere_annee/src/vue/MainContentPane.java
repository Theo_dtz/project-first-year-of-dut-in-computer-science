package vue;

import java.awt.BorderLayout;
import javax.swing.JPanel;
/**
 * Classe qui permet de contenir les autres panels :
 * Le panel Tableau et le panel Graphe
 * 
 * @author Theo
 *
 */
public class MainContentPane extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Le layout manager du MainContentPane
	 */
	private BorderLayout mainContentPaneLayout;
	
	/**
	 * Le panel qui contient le Graphique
	 */
	ChartPane chartPane = new ChartPane();
	
	/**
	 * Le panel qui contient le tableau
	 */
	TablePane tablePane = new TablePane();
	
	/**
	 * Constructeur par defaut de la classe MainContentPane
	 */
	public MainContentPane(){
		
		this.mainContentPaneLayout = new BorderLayout();
		this.tablePane = new TablePane();
		this.chartPane = new ChartPane();
		
		this.add(this.tablePane,BorderLayout.EAST);
		this.add(this.chartPane, BorderLayout.WEST);
	}
	
	
	
	
	
	
	
	
	
	
	
	public ChartPane getChartPane() {
		return chartPane;
	}

	public void setChartPane(ChartPane chartPane) {
		this.chartPane = chartPane;
	}

	public TablePane getTablePane() {
		return tablePane;
	}

	public void setTablePane(TablePane tablePane) {
		this.tablePane = tablePane;
	}

	/**
	 * Getter du layout Mangager de MainContentPane
	 * 
	 * @return MainContentPaneLayout (le Layout de l'objet de la classe MainContentPane)
	 */
	public BorderLayout getMainContentPaneLayout() {
		return mainContentPaneLayout;
	}

	/**
	 * Setter du layout manager de l'objejt mainContentPane
	 * 
	 * @param mainContentPaneLayout
	 */
	public void setMainContentPaneLayout(BorderLayout mainContentPaneLayout) {
		mainContentPaneLayout = mainContentPaneLayout;
	}

}
