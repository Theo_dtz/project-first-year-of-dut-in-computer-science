package vue;

import java.awt.Color;
import javax.swing.JPanel;

/**
 * Classe qui contient le graphique g�n�r� en javaFX
 * 
 * @author Theo
 *
 */
public class ChartPane extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructeur par defaut de la classe ChartPane
	 */
	public ChartPane(){
		
		this.setBackground(Color.CYAN);
	}

}
