package vue;

import java.awt.Dimension;

import javax.swing.JSeparator;
import javax.swing.SwingConstants;

/**
 * Classe qui permet de creer un objet pour separer
 * avec un trait noir deux menus dans la barre de menu 
 * 
 * @author Theo
 *
 */
public class MenuSeparator extends JSeparator {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructeur par default de la classe MenuSeparator
	 */
	public MenuSeparator(){
		
		super(SwingConstants.VERTICAL);
		this.setMaximumSize(new Dimension(2,100));
	}
}
